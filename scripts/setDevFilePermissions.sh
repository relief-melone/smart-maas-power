#!/bin/bash

BASEDIR=$(dirname $0)

if [[ ${BUILD_ENV} == "development" ]]; then
  chmod 660 -R $BASEDIR/../dist/
fi
