#!/bin/sh

if [ "${AUTH_TOKEN_GITLAB}" != "empty" ]; then
  npm config set -- '//gitlab.com/api/v4/packages/npm/:_authToken' "${AUTH_TOKEN_GITLAB}"
fi