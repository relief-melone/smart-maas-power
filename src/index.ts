import express from 'express';

import configMain from './configs/config.main';
import loggerDefault from './log/logger.default';
import routeHealth from './routes/route.health';

import routeMaaspower from './routes/route.maaspower';

export default () => {
  const app = express();

  app.use(express.json());

  // Routes

  app.use('/health', routeHealth);
  app.use('/maaspower', routeMaaspower);

  console.log(configMain);

  app.listen(8080, () => {
    loggerDefault.info('Listening on port 8080');
  });
};