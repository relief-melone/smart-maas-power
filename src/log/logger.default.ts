import winston, { format, transports } from 'winston';
import configLogger from '../configs/config.logger';

const defaultFormat = format.printf(({ level, message, label, timestamp }) => {
  return `[ ${level.toUpperCase()} ]\t ${message} [ ${timestamp} ]`;
});

export const getLogger = (config = configLogger) => {
  const logger = winston.createLogger({
    level: config.logLevel,
    format: format.combine(
      format.timestamp(),
      format.simple(),
      defaultFormat
    ),
    transports: [
      new transports.Console()
    ]

    
  });

  logger.info('Logger successfully initialized');
  logger.info(`LogLevel: ${config.logLevel}`);

  return logger;
};

const defaultLogger = getLogger();
  

export default defaultLogger;