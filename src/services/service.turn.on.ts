import HttpError from '@/classes/HttpError';
import configMain from '@/configs/config.main';
import turnOn from './smart_things/service.turn.on.smart_things';

export const getService = (config = configMain) => 
  async (deviceName: string) => {
    const device = config.getDevice(deviceName);

    if (!device){
      throw new HttpError(404, `${deviceName} not found`);
    }
     

    if (device.type === 'SmartThingDevice')
      return await turnOn(device);

    else 
      throw new HttpError(400, `${device.type} not supported yet`);

    
  };

export default getService();