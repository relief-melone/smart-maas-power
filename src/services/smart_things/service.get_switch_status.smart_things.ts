import { Device } from '@/classes/Config.Main';
import configMain from '@/configs/config.main';
import loggerDefault from '@/log/logger.default';
import smartThingsApi from './service.api';
import getHeaders from './service.api.get_headers.for_device';

export const getService = (config = configMain) =>
  async (device:Device) => {
    
    try {
      await smartThingsApi.post(
        `devices/${device.device_id}/commands`,{
          commands: [{
            component: 'main',
            capability: 'refresh',
            command: 'refresh'
          }]          
        }, {
          headers: getHeaders(device)
        }
      );

      const response = await smartThingsApi.get(
        `devices/${device.device_id}/components/main/capabilities/${device.query}/status`, {
          headers: getHeaders(device)
        }
      );
      
      const status = response.data?.[device.query];

      loggerDefault.info(`Status for device ${device.name} is ${status?.value}`);
      loggerDefault.debug(JSON.stringify(response.data));

      return { status: status.value };
    
    } catch (err) {
      loggerDefault.error(`Could not tun off device ${device.name}`);
      loggerDefault.debug(JSON.stringify(err));
    }

    
  };

export default getService();