import configMain from '@/configs/config.main';
import axios from 'axios';

const getAxiosClient = (config = configMain) => axios.create({
  baseURL: 'https://api.smartthings.com/v1'  
});


export default getAxiosClient();
