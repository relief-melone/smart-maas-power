import { Device } from '@/classes/Config.Main';
import configMain from '@/configs/config.main';
import { AxiosRequestConfig } from 'axios';

export const getService = (config = configMain) => 
  (dev:Device):Record<string,string> => ({
    'authorization': `Bearer ${dev.api_token || config.smartThings.defaultApiToken}`
  });

export default getService();