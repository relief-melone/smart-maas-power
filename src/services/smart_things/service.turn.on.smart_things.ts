import { Device } from '@/classes/Config.Main';
import configMain from '@/configs/config.main';
import loggerDefault from '@/log/logger.default';
import smartThingsApi from './service.api';
import getHeaders from './service.api.get_headers.for_device';

export const getService = (config = configMain) =>
  async (device:Device) => {
    const tupleOn = device.on.split(' ');
    if (tupleOn.length !== 3){
      loggerDefault.error(`Cannot handle on command for device ${device.name}. on does not consist of 3 words`);
      return;
    }
      
    try {
      const response = await smartThingsApi.post(
        `devices/${device.device_id}/commands`,{
          commands: [{
            component: tupleOn[0],
            capability: tupleOn[1],
            command: tupleOn[2]
          }]          
        }, {
          headers: getHeaders(device)
        }
      );
      
      loggerDefault.info(`Successfully turned on device ${device.name}`);
      loggerDefault.debug(JSON.stringify(response.data));
      
    
    } catch (err) {
      loggerDefault.error(`Could not tun on device ${device.name}`);
      loggerDefault.debug(JSON.stringify(err));
    }

    
  };

export default getService();