import HttpError from '@/classes/HttpError';
import configMain from '@/configs/config.main';
import turnOff from './smart_things/service.turn.off.smart_things';


export const getService = (config = configMain) => 
  async (deviceName: string) => {
    const device = config.getDevice(deviceName);

    if (!device){
      throw new HttpError(404, `${deviceName} not found`);
    }
     

    if (device.type === 'SmartThingDevice')
      return await turnOff(device);

    else 
      throw new HttpError(400, `${device.type} not supported yet`);

    
  };

export default getService();