import { Logger } from 'winston';

export interface MandatoryEnvVarOpts {
  message?: string
  mandatoryInProduction?: boolean,
  disableLogging?: boolean,
  logger?: any
}

export const shouldLog = (opts:MandatoryEnvVarOpts, logger:any):boolean => 
  !opts.disableLogging && logger;

export const getEnvVar = (env = process.env) => 
  (
    envVarName:string, 
    defaultValue?: string, 
    opts: MandatoryEnvVarOpts = {},    
  ):string =>{
    const logger = opts.disableLogging 
      ? null
      : opts.logger === undefined ? (require('../log/logger.default') || null)?.getLogger() as Logger : null;
    
    if (!logger)
      opts.disableLogging = true;


    const val = env[envVarName] || defaultValue;
    
    if (!val){
      if (logger)
        logger.error(opts.message || `PLEASE PROVIDE ENV VAR ${envVarName}`);
      process.exit(1);
    }

    if (!env[envVarName])
      if (opts.mandatoryInProduction && env.NODE_ENV==='production'){
        if (logger)
          logger.error(opts.message || `A DEFAULT VALUE FOR ${envVarName} HAS BEEN PROVIDED HOWEVER IN PRODUCTION A VALUE HAS TO BE SET`);
        process.exit(1);
      }
      else {
        if (opts.mandatoryInProduction && logger){
          if (!opts.disableLogging)
            logger.warn(`${envVarName} has been set to ${defaultValue}. Default value is not allowed in production`);
        } else {
          if (logger)
            logger.info(`${envVarName} has been set to ${defaultValue}`);
        }
      }

    return val;
  };

export default getEnvVar();