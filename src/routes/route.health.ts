import { Router } from 'express';

export const getRoute = ():Router => Router()
  .get('/', (_req, res) => res.status(200).send('OK'));

export default getRoute();