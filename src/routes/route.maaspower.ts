import { Router } from 'express';

import webhookController from '@/controllers/controller.webhook';
import checkAuthController from '@/controllers/controller.check_authentication';

export const getRoute = ():Router => Router()
  .use(checkAuthController)
  .post('/:deviceName/:command', webhookController)
  .get('/:deviceName/:command', webhookController);

export default getRoute();