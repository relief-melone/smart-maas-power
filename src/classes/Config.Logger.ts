import Config from './Config';

export class ConfigLogger extends Config {  
  constructor(){
    super();
  }

  get logLevel():string {
    return this.env('LOG_LEVEL', 'info', {
      disableLogging: true,
      logger: null
    });
  }
}