export default class HttpError extends Error {

  code: number;
  message: string;
  details: Record<string,any>;
  
  constructor(code: number, message: string, details = {}){
    super();

    this.code = code;
    this.message = message;
    this.details = details;
  }
}