import Config from './Config';
import fs from 'fs';
import yaml from 'yaml';
import path from 'path';

import rs from 'randomstring';
import loggerDefault from '@/log/logger.default';

export type DeviceType = 'SmartThingDevice' | 'CommandLine' | 'WebGui';

export interface Device {
  type: DeviceType
  name: string
  api_token: string
  device_id: string
  off: string
  on: string
  query: string
}

export default class ConfigMain extends Config {

  devices: Array<Device>;
  configPath: string;
  file: string | null;
  auth: {
    username: string;
    password: string;
  };
  fileCheckIntervalSeconds: number;

  smartThings: {
    defaultApiToken: string | null;
  };

  constructor(env = process.env) {
    super(env);
    this.configPath = this.env('CONFIG_FILE_PATH', `${__dirname}/../../../test/resources/config.yaml`);
    this.file = this.updateFile();
    this.devices = this.initDevices();
    this.smartThings = this.initSmartThings();

    this.auth = {
      username: this.env('USERNAME', 'power_user', { mandatoryInProduction: true }),
      password: this.env('PASSWORD', rs.generate({ length: 26 }), {
        mandatoryInProduction: true
      })
    };
    this.fileCheckIntervalSeconds = parseInt(this.env('FILE_CHECK_CHANGES_INTERVAL_SECONDS', '5'));

    
    setInterval(() => {
      const currentFile = this.file;
      this.updateFile();
      if (this.file !== currentFile){
        loggerDefault.info('Config file change detected. Applying');
        this.initDevices();
        this.initSmartThings();
      }
    });

  }

  updateFile(){
    this.file = fs.existsSync(this.configPath) ? fs.readFileSync(this.configPath, { encoding: 'utf-8' }) : null;
    return this.file;
  }

  initDevices():this['devices'] {
    this.devices = this.file 
      ? yaml.parse(this.file).devices
      : [];
    return this.devices;
  }

  initSmartThings(): this['smartThings']{
    this.smartThings = {
      defaultApiToken: this.file 
        ? yaml.parse(this.file).smartThings?.default_api_token || process.env['SMART_THINGS__DEFAULT_API_TOKEN'] || null
        : process.env['SMART_THINGS__DEFAULT_API_TOKEN'] || null
    };
    return this.smartThings;
  }

  getDevice(deviceName:string):Device|null {
    const resultByName = this.devices.find(dev => dev.name === deviceName);
    
    if (resultByName){
      loggerDefault.info(`Found device ${resultByName.name} by its name`);
      return resultByName;
    }

    const resultById = this.devices.find(dev => dev.device_id === deviceName);

    if (resultById){
      loggerDefault.info(`Found device ${resultById.name} by its id`);
      return resultById;
    }

    loggerDefault.info(`Device ${deviceName} could not be found`);      
    return null;
  }

  
}