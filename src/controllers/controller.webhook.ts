import HttpError from '@/classes/HttpError';
import loggerDefault from '@/log/logger.default';

import { RequestHandler } from 'express';
import controllerErrors from './controller.errors';
import webhookControllerGET from './controller.webhook.get';
import webhookControllerPOST from './controller.webhook.post';

export const getController = ():RequestHandler => 
  async (req, res, next) => {

    const { deviceName, command } = req.params;
    loggerDefault.info(`Handling command ${command} for deviceName ${deviceName}`);

    switch (req.method){
      case 'POST':
        return webhookControllerPOST(req,res,next);
      case 'GET':
        return webhookControllerGET(req,res,next);
      default:
        const err = new HttpError(400, `Method ${req.method} currently has no supported endpoints`);
        loggerDefault.error(err.message);
        controllerErrors(err, res);
    }

  };

const webhookController = getController();

export default webhookController;