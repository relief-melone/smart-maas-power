import HttpError from '@/classes/HttpError';
import loggerDefault from '@/log/logger.default';
import serviceTurnOff from '@/services/service.turn.off';
import serviceTurnOn from '@/services/service.turn.on';
import { RequestHandler } from 'express';
import controllerErrors from './controller.errors';

export const getController = ():RequestHandler => 
  async (req, res, next) => {

    const { deviceName, command } = req.params;
    loggerDefault.info(`Handling command ${command} for deviceName ${deviceName}`);


    try {
      switch (command){
        
        case 'on':
          await serviceTurnOn(deviceName);
          return res.status(200).json({ deviceName, command, success: true, status: 'on' });

        case 'off':
          await serviceTurnOff(deviceName);
          return res.status(200).json({ deviceName, command, success: true, status: 'off' });
        default:
          const err = new HttpError(400, `Command ${command} currently not supported with a POST request`, {
            msg: 'supported commands are [ on, off ]'
          });
          loggerDefault.error(err.message);
          controllerErrors(err, res);

      }
    } catch (err) {
      return controllerErrors(err, res);
    }
  };

const webhookControllerPOST = getController();

export default webhookControllerPOST;