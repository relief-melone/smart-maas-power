import HttpError from '@/classes/HttpError';
import loggerDefault from '@/log/logger.default';
import serviceGetStatus from '@/services/service.status.get';
import { RequestHandler } from 'express';
import controllerErrors from './controller.errors';

export const getController = ():RequestHandler =>
  async (req, res, next) => {
    const { deviceName, command } = req.params;
    loggerDefault.info(`Handling command ${command} for deviceName ${deviceName}`);


    try {
      switch (command){        
        case 'status':
          const status = await serviceGetStatus(deviceName);
          return res.status(200).json(status);
        default:
          const err = new HttpError(400, `Command ${command} currently not supported with a GET request`, {
            msg: 'supported commands are [ status ]'
          });
          loggerDefault.error(err.message);
          controllerErrors(err, res);

      }
    } catch (err) {
      return controllerErrors(err, res);
    }
  };

export const webhookControllerGET = getController();

export default webhookControllerGET;