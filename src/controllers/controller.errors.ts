import HttpError from '@/classes/HttpError';
import { Response } from 'express';

export const getController = () => 
  (err: any, res: Response) => {
    if (err instanceof HttpError){
      return res.status(err.code).send(err);
    }
    return res.status(500).send('Internal Server Error');
  };

export default getController();