import configMain from '@/configs/config.main';
import { RequestHandler } from 'express';

import errorController from './controller.errors';
import HttpError from '@/classes/HttpError';
import loggerDefault from '@/log/logger.default';

const getController = (config = configMain ):RequestHandler => 
  (req, res, next) => {
    const auth = req.headers.authorization;
    if (!auth){
      const err = new HttpError(403, 'unauthorized', { msg: 'Authorization header missing' });
      return errorController(err, res);
    }

    const [user, password] = Buffer.from(auth.replace('Basic ', '').trim(), 'base64').toString().split(':');
    loggerDefault.debug(`Handling request for user ${user}`);
    
    if (user !== config.auth.username || password !== config.auth.password){
      const err = new HttpError(403, 'unauthorized', { msg: 'wrong username password combination' });
      return errorController(err, res);
    }

    next();

  };

export default getController();
