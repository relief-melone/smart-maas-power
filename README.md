# SMART-MAASPOWER

Simple Webserver to receive webhooks that can be used in Canonicals MAAS to use Wifi SmartSockets. Every Socket that can be connected to Samsung SmartThings should be compatible however this has been tested with Tapo P100 sockets as welkl as Gosund EP2s.

The goal is to provide a Tool that is cloud native and can easily be deployed in kubernetes.

The project is heavily inspired by [maaspower](https://gilesknap.github.io/maaspower) and was only created due to the fact that it stopped working in my setup for some time.

It relies on SmartThings RestAPI instead of the Python library

Smart Maaspower currently only supports SmartThings (hence the name) and no other controllers like smartmaas does. Additionaly it come

## Configuration

Section will be updated. for now see ci/helm/values.yaml as config example and play around with your own values file

## Installation

This installation assumes you have created your own config file *my-values.yaml*

```sh
helm install maaspower -n maaspower --create-namespace ./ci/helm -f ./my-values.yaml
```


