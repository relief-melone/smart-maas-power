ARG NODE_VERSION=18

FROM node:${NODE_VERSION}-slim as builder

LABEL maintainer="relief.melone [ relief.melone@gmail.com ]"

ARG BUILD_ENV=production
ARG CI_JOB_TOKEN

WORKDIR /app

RUN apt-get update && \
    apt-get install -y git

RUN git clone --depth=1 https://gitlab.com/rm-templates/common-scripts.git && \
    rm -rf ./common_scripts/.git

COPY ./package*.json ./.npmrc ./
COPY ./scripts ./scripts

RUN ./common-scripts/npm/gitlab.add_gitlab_npm_registry.sh && \
    npm i

COPY . . 

RUN npm run build && \
    ./scripts/setDevFilePermissions.sh



FROM node:${NODE_VERSION}-alpine

ARG USER_ID=1001
ARG GROUP_ID=1001
ARG USER_NAME=app

WORKDIR /app

COPY --from=builder /app /app

RUN addgroup -S -g $GROUP_ID $USER_NAME && \
    adduser -S $USER_NAME $USER_NAME
RUN chown -R $USER_NAME:$USER_NAME /app 

USER $USER_NAME

EXPOSE 8080
EXPOSE 9229

ENTRYPOINT [ "/bin/sh" ]
CMD ["./scripts/start.sh"]
