import moduleAlias from 'module-alias';

moduleAlias.addAliases({
  '@': `${__dirname}/src`,
  '@test': `${__dirname}/test`,
  '@root': `${__dirname}`
});


import app from '@/index';

app();

export default app;