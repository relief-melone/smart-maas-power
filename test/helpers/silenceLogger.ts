import defaultLogger from '@/log/logger.default';

export default () => {
  const log = console.log;

  beforeEach(() => {
    defaultLogger.silent = true;
    console.log = () => {};
  });

  afterEach(() => {
    defaultLogger.silent = false;
    console.log = log;
  });
};